using System;
using Xunit;

namespace FizzBuzz.Tests
{
    public class FizzBuzzEvaluatorTests
    {
        FizzBuzzEvaluator evaluator;
        public FizzBuzzEvaluatorTests(){
            evaluator = new FizzBuzzEvaluator();
        }

        [Fact]
        public void ForNonFizzOrBuzzMultipleIsTheNumberAsString()
        {
            Assert.Equal("1", evaluator.Evaluate(1));
        }

        [Fact]
        public void ForMultipleOfThreeIsFizz() {
            Assert.Equal(
                "fizz", evaluator.Evaluate(3));
        }

        [Fact]
        public void ForMultipleOfFiveIsBuzz()
        {
            Assert.Equal(
                "buzz", evaluator.Evaluate(5));
        }
    }
}
